package com.example.game;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.managers.UuidEntityManager;
import com.artemis.utils.Bag;
import java.util.UUID;

public class ArtemisUuidTest {

  public static UUID TEST_UUID = UUID.randomUUID();

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    WorldConfiguration configuration = new WorldConfiguration();
    configuration.setManager(UuidEntityManager.class);
    configuration.setManager(EntityTracker.class);

    configuration.setSystem(DummySystem.class);

    World world = new World(configuration);
    world.createEntity(TEST_UUID);
    world.process();

    doSomething(world);

    world.process();
    world.process();
  }

  private static void doSomething(World world) {
    Bag<Entity> entities = world.getManager(EntityTracker.class).getEntities();
    for (Entity entity : entities) {
      world.deleteEntity(entity);
    }
    world.createEntity(TEST_UUID);
    System.out.println(world.getManager(UuidEntityManager.class).getEntity(TEST_UUID));
  }
}
