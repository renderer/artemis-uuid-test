package com.example.game;

import com.artemis.Entity;
import com.artemis.Manager;
import com.artemis.utils.Bag;

/**
 *
 * @author implicit-invocation
 */
public class EntityTracker extends Manager {

  private final Bag<Entity> entities = new Bag<Entity>();

  @Override
  public void added(Entity e) {
    entities.add(e);
  }

  @Override
  public void deleted(Entity e) {
    entities.remove(e);
  }
  
  public Bag<Entity> getEntities() {
    return entities;
  }

}
