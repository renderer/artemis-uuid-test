package com.example.game;

import com.artemis.BaseSystem;
import com.artemis.managers.UuidEntityManager;

/**
 *
 * @author implicit-invocation
 */
public class DummySystem extends BaseSystem {

  @Override
  protected void processSystem() {
    System.out.println(world.getManager(UuidEntityManager.class).getEntity(ArtemisUuidTest.TEST_UUID));
  }

}
